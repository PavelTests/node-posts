const express = require('express');
const {
    getPosts,
    deletePostsId,
    getPostsId,
    getEditPostId,
    putEditPostId,
    getAddPost,
    postAddPost,
} = require('../controllers/post-controller');

const router = express.Router();

router.get('/posts', getPosts);

router.delete('/posts/:id', deletePostsId);

router.get('/posts/:id', getPostsId);

router.get('/edit-post/:id', getEditPostId);

router.put('/edit-post/:id', putEditPostId);

router.get('/add-post', getAddPost);

router.post('/add-post', postAddPost);

module.exports = router;