const express = require('express');
const {
    getPosts,
    deletePostsId,
    getPostsId,
    putEditPostId,
    postAddPost,
} = require('../controllers/api-post-controller');

const router = express.Router();

router.get('/api/posts', getPosts);

router.get('/api/posts/:id', getPostsId);

router.delete('/api/posts/:id', deletePostsId);

router.post('/api/add-post', postAddPost);

router.put('/api/edit-post/:id', putEditPostId);

module.exports = router;