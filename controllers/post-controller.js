const { Post } = require('../models/post');
const createPath = require('../methods/methods');

// const post = {
//     title,
//     text,
//     author,
//     id: new Date(),
//     date: new Date().toLocaleDateString(),
// }

const getPosts = (req, res) => {
    const title = 'Posts';
    Post
        .find()
        .sort({ createdAt: -1 })
        .then(posts => res.render(createPath('posts'), { title, posts }))
        .catch(err => {
            console.log(err);
            const title = 'Error'
            res
                .status(404)
                .render(createPath('error'), { title })
        })
}

const deletePostsId = (req, res) => {
    Post
        .findByIdAndDelete(req.params.id)
        .then(() => res.sendStatus(200))
        .catch(err => {
            console.log(err);
            const title = 'Error';
            res
                .status(404)
                .render(createPath('error'), { title });
        })
}

const getPostsId = (req, res) => {
    const title = 'Post';
    Post
        .findById(req.params.id)
        .then(post => {
            if (!post) throw new Error('Null post');
            res.render(createPath('post'), { title, post })
        })
        .catch(err => {
            console.log(err);
            const title = 'Error';
            res
                .status(404)
                .render(createPath('error'), { title });
        })
}

const getEditPostId = (req, res) => {
    const title = 'Edit post';
    Post
        .findById(req.params.id)
        .then(post => {
            if (!post) throw new Error('Null post');
            res.render(createPath('edit-post'), { title, post })
        })
        .catch(err => {
            console.log(err);
            const title = 'Error';
            res
                .status(404)
                .render(createPath('error'), { title });
        })
}

const putEditPostId = (req, res) => {
    const id = req.params.id;
    const { title, author, text } = req.body;
    Post
        .findByIdAndUpdate(id, { title, author, text }, { new: true })
        .then(post => {
            if (!post) throw new Error('Null post');
            const title = 'Post'
            res.render(createPath('post'), { title, post })
        })
        .catch(err => {
            console.log(err);
            const title = 'Error';
            res
                .status(404)
                .render(createPath('error'), { title });
        })
}

const getAddPost = (req, res) => {
    const title = 'Add post';
    res.render(createPath('add-post'), { title });
}

const postAddPost = (req, res) => {
    console.log(req.url);
    const { title, text, author } = req.body;

    const post = new Post({ title, text, author });
    post
        .save()
        .then(() => res.redirect('/posts'))
        .catch(err => {
            console.log(err);
            const title = 'Error'
            res
                .status(404)
                .render(createPath('error'), { title })
        })
}

module.exports = {
    getPosts,
    deletePostsId,
    getPostsId,
    getEditPostId,
    putEditPostId,
    getAddPost,
    postAddPost,
}