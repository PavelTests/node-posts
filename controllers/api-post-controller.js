const { Post } = require('../models/post');

// const post = {
//     title,
//     text,
//     author,
//     id: new Date(),
//     date: new Date().toLocaleDateString(),
// }

const handleError = (res, error) => {
    res.status(404);
    console.log(error);
}

const getPosts = (req, res) => {
    Post
        .find()
        .sort({ createdAt: -1 })
        .then(posts => res.status(200).json(posts))
        .catch(err => handleError(res, err));
}

const deletePostsId = (req, res) => {
    Post
        .findByIdAndDelete(req.params.id)
        .then(() => res.status(200).json(req.params.id))
        .catch(err => handleError(res, err));
}

const getPostsId = (req, res) => {
    Post
        .findById(req.params.id)
        .then(post => {
            if (!post) throw new Error('Null post');
            res.status(200).json(post);
        })
        .catch(err => handleError(res, err));
}

const putEditPostId = (req, res) => {
    const id = req.params.id;
    const { title, author, text } = req.body;
    Post
        .findByIdAndUpdate(id, { title, author, text }, { new: true })
        .then(post => {
            if (!post) throw new Error('Null post');
            res.status(200).json(post);
        })
        .catch(err => handleError(res, err));
}

const postAddPost = (req, res) => {
    const { title, text, author } = req.body;

    const post = new Post({ title, text, author });
    post
        .save()
        .then(post => res.status(200).json(post))
        .catch(err => handleError(res, err));
}

module.exports = {
    getPosts,
    deletePostsId,
    getPostsId,
    putEditPostId,
    postAddPost,
}