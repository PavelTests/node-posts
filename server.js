const postRoutes = require('./routes/post-routes');
const postApiRoutes = require('./routes/api-post-routes');
const createPath = require('./methods/methods');

const cors = require('cors');
const express = require('express');
const app = express();
const morgan = require('morgan');
require('dotenv').config();
const mongoose = require('mongoose');
const methodOverride = require('method-override');

//mongoDB
mongoose
    .connect(process.env.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then((res) => console.log('Connect MongoDB'))
    .catch((er) => console.log(er))

//engine for valid HTML output to browser
app.set('view engine', 'ejs');

app.listen(process.env.PORT, process.env.HOST, er => {
    er ? console.log(er) : console.log('Server started');
});

app.use(cors({origin: 'http://localhost:4200'}));
//for access to static files in the /styles from /views
app.use(express.static('styles'));

//for logger in console
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

//for decoding data in POST/PUT request
app.use(express.urlencoded({ extended: false }));

//for detecting PUT method in the http//..._method=PUT request
app.use(methodOverride('_method'));

app.get('/', (req, res) => {
    // res.send('<h1>Hello</h1>');
    const title = 'Home';
    res.render(createPath('index'), { title });
});

//posts-related routes logic
app.use(postRoutes);

//api-posts-related routes logic
app.use(postApiRoutes);

app.get('/contacts', (req, res) => {
    const title = 'Contacts';
    res.render(createPath('contacts'), { title });
});

app.use((req, res) => {
    const title = 'Error';
    res.status(404)
        .render(createPath('error'), { title });
})